package com.itzhi.linkedlist;

import java.util.Stack;

/*
* 单向链表
* */
public class SingleLinkedListDemo {
    public static void main(String[] args) {

        HeroNode hero1 = new HeroNode(1,"宋江","及时雨");
        HeroNode hero2 = new HeroNode(2,"卢俊义","玉麒麟");
        HeroNode hero3 = new HeroNode(3,"吴用","智多星");
        HeroNode hero4 = new HeroNode(4,"林冲","豹子头");

        SingleLinedList singleLinedList = new SingleLinedList();
        //添加
        singleLinedList.add(hero1);
        singleLinedList.add(hero2);
        singleLinedList.add(hero3);
        singleLinedList.add(hero4);

//        singleLinedList.list();
//        System.out.println();
//        reverseList(singleLinedList.getHead());

//        singleLinedList.list();
//        reversePrint(singleLinedList.getHead());


//        singleLinedList.addByOrder(hero1);
////        singleLinedList.addByOrder(hero3);
//        singleLinedList.addByOrder(hero2);
//        singleLinedList.addByOrder(hero3);
//        singleLinedList.addByOrder(hero4);
//
//        singleLinedList.list();
//        System.out.println("--------------------------------------------");
//
//        //修改
//        HeroNode newHeroNode = new HeroNode(2,"栓困","冯鹏飞");
//        singleLinedList.update(newHeroNode);
//
//        singleLinedList.list();
//        System.out.println("-----------------------------------------------");
//
//        singleLinedList.del(1);
//        singleLinedList.list();
//
//        System.out.println("有效个数为："+getLength(singleLinedList.getHead()));
//
//        HeroNode res = findLastIndexNode(singleLinedList.getHead(),4);
//        System.out.println(res);

        SingleLinedList singleLinedList3 = new SingleLinedList();

        HeroNode hero5 = new HeroNode(5,"宋江","及时雨");
        HeroNode hero6 = new HeroNode(6,"卢俊义","玉麒麟");
        HeroNode hero7 = new HeroNode(7,"吴用","智多星");
        HeroNode hero8 = new HeroNode(8,"林冲","豹子头");

        SingleLinedList singleLinedList2 = new SingleLinedList();
        //添加
        singleLinedList2.add(hero5);
        singleLinedList2.add(hero6);
        singleLinedList2.add(hero7);
        singleLinedList2.add(hero8);

        twoLinkedList(singleLinedList.getHead(),singleLinedList2.getHead(),singleLinedList3.getHead());
        singleLinedList3.list();

    }

    //获取单链表有效节点的个数 不包含头节点
    public static int getLength(HeroNode head){
        if (head.next == null){//空链表
            return 0;
        }
        int length = 0;
        HeroNode cur = head.next;//定义一个辅助变量  没有统计头节点
        while (cur != null){
            length++;    
            cur = cur.next;
        }
        return length;
    }

    //查找单链表的倒数第index节点
    public static HeroNode findLastIndexNode(HeroNode head, int index){
        if (head.next == null){//判空
            return null;
        }
        int size = getLength(head);
        if (index <= 0 || index > size){
            return null;
        }
        HeroNode temp = head.next;
        for (int i = 0;i < size - index;i++){
            temp = temp.next;
        }
        return temp;
    }

    // 反转单链表
    public static void reverseList(HeroNode head){
        if (head.next == null || head.next.next == null){
            return;
        }

        //定义一个辅助的指针 帮助我们遍历原来的链表
        HeroNode cur = head.next;
        HeroNode next = null;//指向当前节点cur 的下一个节点
        HeroNode reverseHead = new HeroNode(0,"","");
        //遍历原来的链表 每遍历一个节点 就将其取出，并放在新的链表的最前端
        while (cur != null){
            next = cur.next;//先暂时保存当前节点的下一个节点
            cur.next = reverseHead.next;//将cur 的下一个节点指向新的链表的最前端
            reverseHead.next = cur;//将cur连接到新的链表上
            cur = next;//让cur后移
        }
        //实现单链表的反转
        head.next = reverseHead.next;
    }

    // 逆序输出链表 没有改变链表的结构
    public static void reversePrint(HeroNode head){
        if (head.next == null){
            return;
        }
        Stack<HeroNode> stack = new Stack<HeroNode>();

        HeroNode cur = head.next;
        while (cur != null){
            stack.push(cur);
            cur = cur.next;
        }
        while (stack.size() > 0){
            System.out.println(stack.pop());
        }
    }

    public static void twoLinkedList(HeroNode head1, HeroNode head2, HeroNode head3) {
        // 如果两个链表均为空，则无需合并，直接返回
        if(head1.next == null && head2.next == null) {
            return;
        }
        // 如果链表1为空，则将head3.next指向head2.next，实现链表2中的节点连接到链表3
        if(head1.next == null) {
            head3.next = head2.next;
        } else {
            // 将head3.next指向head1.next，实现链表1中的节点连接到链表3
            head3.next = head1.next;
            // 定义一个辅助的指针（变量），帮助我们遍历链表2
            HeroNode cur2 = head2.next;
            // 定义一个辅助的指针（变量），帮助我们遍历链表3
            HeroNode cur3 = head3;
            HeroNode next = null;
            // 遍历链表2，将其节点按顺序连接至链表3
            while(cur2 != null) {
                // 链表3遍历完毕后，可以直接将链表2剩下的节点连接至链表3的末尾
                if(cur3.next == null) {
                    cur3.next = cur2;
                    break;
                }
                // 在链表3中，找到第一个大于链表2中的节点编号的节点
                // 因为是单链表，找到的节点是位于添加位置的前一个节点，否则无法插入
                if(cur2.no <= cur3.next.no) {
                    next = cur2.next;  // 先暂时保存链表2中当前节点的下一个节点，方便后续使用
                    cur2.next = cur3.next;  // 将cur2的下一个节点指向cur3的下一个节点
                    cur3.next = cur2;  // 将cur2连接到链表3上
                    cur2 = next;  // 让cur2后移
                }
                // 遍历链表3
                cur3 = cur3.next;
            }
        }
    }

}

//定义单链表 管理
class SingleLinedList{
    //先初始化一个头节点 头节点不要动，不存放具体的数据
    private HeroNode head = new HeroNode(0,"","");

    public HeroNode getHead() {
        return head;
    }

    //添加节点到单向链表
    //1、找到最后一个节点
    //2、将最后一个节点的next指向新的节点
    public void add(HeroNode heroNode){
        //因为head节点不能动，所以我们需要一个辅助遍历
        HeroNode temp = head;
        //遍历链表 找到最后一个节点
        while (true){
            //找到链表的最后
            if(temp.next == null){
                break;
            }
            //如果没有找到 将temp后移
            temp = temp.next;
        }
        //当退出while循环时，temp就指向最后一个节点
        //将最后这个节点指向新的节点
        temp.next = heroNode;
    }

    public void update(HeroNode newHeroNode){
        HeroNode temp = head.next;
        boolean flag = false;
        while (true){
            if(temp == null){
                break;
            }
            if(temp.no == newHeroNode.no){
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if(flag){
            temp.name = newHeroNode.name;
            temp.nickname = newHeroNode.nickname;
        }else{
            System.out.println("没有找到编号为"+newHeroNode.no+"的节点，不能修改");
        }
    }

    public void del(int no){
        HeroNode temp = head;
        boolean flag = false;
        while (true){
            if(temp.next == null){
                break;
            }
            if (temp.next.no == no){
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if(flag){
            temp.next = temp.next.next;
        }else{
            System.out.println("链表为空！");
        }
    }

    //将排名英雄插入到指定位置
    public void addByOrder(HeroNode heroNode){
        //因为是单链表，因此我们找的temp是位于添加位置的前一个节点，否则插入不了
        HeroNode temp = head;//标志添加的编号是否存在
        boolean flag = false;
        while (true){
            if(temp.next == null){//说明temp已经在链表的最后
                break;
            }
            if(temp.next.no > heroNode.no){//位置找到，就在temp的后面加入
                break;
            }else if(temp.next.no == heroNode.no){//说明希望添加的编号已经存在
                flag = true;
                break;
            }
            temp = temp.next;//后移，遍历当前链表
        }
        if(flag){
            System.out.println("准备插入的编号"+heroNode.no+"已经存在，不能加入");
        }else{
            heroNode.next = temp.next;
            temp.next = heroNode;
        }

    }

    //显示链表
    public void list(){
        //判断链表是否为空
        if (head.next == null){
            System.out.println("该链表为空");
            return;
        }
        HeroNode temp = head.next;
        while (true){
            //判断是否到最后一个节点
            if (temp == null){
                break;
            }
            //输出节点的信息
            System.out.println(temp);
            //将temp后移
            temp = temp.next;
        }
    }
}


//每个HeroNode对象就是一个节点
class HeroNode{

    public int no;
    public String name;
    public String nickname;
    public HeroNode next;//指向下一个节点

    public HeroNode(int no, String name, String nickname) {
        this.no = no;
        this.name = name;
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "HeroNode{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
