package com.itzhi.linkedlist;

/*
* 双向链表
* */
public class DoubleLinkedListDemo {
    public static void main(String[] args) {
        HeroNode2 hero1 = new HeroNode2(1,"宋江","及时雨");
        HeroNode2 hero2 = new HeroNode2(2,"卢俊义","玉麒麟");
        HeroNode2 hero3 = new HeroNode2(3,"吴用","智多星");
        HeroNode2 hero4 = new HeroNode2(4,"林冲","豹子头");

        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();

        doubleLinkedList.add(hero1);
        doubleLinkedList.add(hero2);
        doubleLinkedList.add(hero3);
        doubleLinkedList.add(hero4);
        doubleLinkedList.list();

        HeroNode2 hero = new HeroNode2(4,"ddc","豹子头");
        doubleLinkedList.update(hero);
        System.out.println();
        doubleLinkedList.list();
        System.out.println();

        doubleLinkedList.del(3);
        doubleLinkedList.list();
        System.out.println();

        HeroNode2 hero5 = new HeroNode2(2,"ddcasdasd","豹子头");
        doubleLinkedList.addByOrder(hero5);
        doubleLinkedList.list();

    }
}

class DoubleLinkedList{

    private HeroNode2 head = new HeroNode2(0,"","");

    public HeroNode2 getHead(){

        return head;
    }

    public void add(HeroNode2 heroNode2){

        HeroNode2 temp = head;
        while (true){
            if (temp.next == null){
                break;
            }
            temp = temp.next;
        }

        temp.next = heroNode2;
        heroNode2.pre = temp;
    }

    public void update(HeroNode2 newHeroNode){

        if (head.next == null){
            System.out.println("链表为空");
            return;
        }

        HeroNode2 temp = head.next;
        boolean flag = false;
        while (true) {
            if (temp == null) {
                break;
            }
            if (temp.no == newHeroNode.no) {
                flag = true;
                break;
            }
            temp = temp.next;
        }
        if (flag) {
            temp.name = newHeroNode.name;
            temp.nickname = newHeroNode.nickname;
        } else {
            System.out.println("没有找到编号为"+newHeroNode.no+"的节点，不能修改");
        }
    }

    public void del(int no){

        if (head.next == null){
            System.out.println("链表为空，无法删除");
            return;
        }

        HeroNode2 temp = head.next;
        boolean flag = false;
        while (true){
            if (temp.next == null){
                break;
            }
            if (temp.next.no == no){
                flag = true;
                break;
            }
            temp = temp.next;
        }

        if (flag){
            temp.pre.next = temp.next;
            if (temp.next != null){
                temp.next.pre = temp.pre;
            }
        } else {
            System.out.println("要删除的" + no + "节点不存在");
        }

    }

    //将排名英雄插入到指定位置
    public void addByOrder(HeroNode2 heroNode) {
        //因为是单链表，因此我们找的temp是位于添加位置的前一个节点，否则插入不了
        HeroNode2 temp = head;//标志添加的编号是否存在
        boolean flag = false;
        while (true) {
            if (temp.next == null) {//说明temp已经在链表的最后
                break;
            }
            if (temp.next.no > heroNode.no) {//位置找到，就在temp的后面加入
                break;
            } else if (temp.next.no == heroNode.no) {//说明希望添加的编号已经存在
                flag = true;
                break;
            }
            temp = temp.next;//后移，遍历当前链表
        }
        if (flag) {
            System.out.println("准备插入的编号" + heroNode.no + "已经存在，不能加入");
        } else {
            heroNode.next = temp.next;
            temp.next.pre = heroNode;
            heroNode.pre = temp;
            temp.next = heroNode;
        }

    }
    public void list(){

        if (head.next == null){
            System.out.println("链表为空");
            return;
        }

        HeroNode2 temp = head.next;
        while(true) {
            if (temp == null){
                break;
            }
            System.out.println(temp);
            temp = temp.next;
        }
    }

}

//每个HeroNode对象就是一个节点
class HeroNode2{

    public int no;
    public String name;
    public String nickname;
    public HeroNode2 next;//指向下一个节点
    public HeroNode2 pre;//指向前一个节点

    public HeroNode2(int no, String name, String nickname) {
        this.no = no;
        this.name = name;
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "HeroNode2{" +
                "no=" + no +
                ", name='" + name + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}