package com.itzhi.linkedlist;

/*
* 约瑟夫问题
* */
public class JoseFu {
    public static void main(String[] args) {

        CircleSingleLinkedList linkedList = new CircleSingleLinkedList();
        linkedList.addBoy(5);
        linkedList.showBoy();

        linkedList.countBoy(1,2,5);
    }
}

// 创建一个环形的单向链表
class CircleSingleLinkedList{

    private Boy first = null;

    public void addBoy(int nums){

        if (nums < 1){
            System.out.println("nums的值不正确");
            return;
        }

        Boy curBoy = null;
        for (int i = 1;i <= nums;i++){
            Boy boy = new Boy(i);
            if (i == 1){
                first = boy;
                first.setNext(first);
            } else {
                curBoy.setNext(boy);
                boy.setNext(first);
            }
            curBoy = boy;
        }
    }

    public void showBoy() {

        if (first == null){
            System.out.println("没有小孩");
            return;
        }

        Boy curBoy = first;
        while (true) {
            System.out.println("编号为：" + curBoy.getNo());
            if (curBoy.getNext() == first){
                break;
            }
            curBoy = curBoy.getNext();
        }
    }

    // 计算出圈的顺序
    public void countBoy(int startNo,int countNum,int nums){
        if (first == null || startNo < 1 || startNo >nums) {
            System.out.println("参数输入有误！");
            return;
        }

        // 指向最后一个节点
        Boy helper = first;
        while (true) {
            if (helper.getNext() == first){
                break;
            }
            helper = helper.getNext();
        }
        // 报数前，让helper 和 first移动k-1次
        for (int i = 0; i < startNo - 1;i++){
            first = first.getNext();
            helper = helper.getNext();
        }
        // 报数时，移动m - 1次
        while (true) {
            if (helper == first){
                break;
            }

            for (int j = 0;j < countNum - 1;j++){
                first = first.getNext();
                helper = helper.getNext();
            }
            System.out.println(first.getNo() + "out");
            // 出圈
            first = first.getNext();
            helper.setNext(first);
        }
        // 输出最后一个节点
        System.out.println(helper.getNo() + "lastOut");
    }

}


class Boy{

    private int no;
    private Boy next;

    public Boy(int no){
        this.no = no;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public Boy getNext() {
        return next;
    }

    public void setNext(Boy next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "Boy{" +
                "no=" + no +
                ", next=" + next +
                '}';
    }
}


