package com.itzhi.queue;

import java.util.Scanner;

/*
* 只能使用一次，没有达到复用的效果  改进成一个环形的队列
* */
public class ArrayQueueDemo {

    public static void main(String[] args) {

        ArrayQueue queue = new ArrayQueue(3);
        char key = ' ';
        Scanner input = new Scanner(System.in);
        boolean loop  =true;
        while (loop){
            System.out.println("s(show):显示队列");
            System.out.println("e(exit):退出队列");
            System.out.println("g(get):获取队列");
            System.out.println("a(add):添加队列");
            System.out.println("h(head):查看头部");

            key = input.next().charAt(0);

            switch (key){
                case 's':
                    queue.shoeQueue();
                    break;
                case 'a':
                    System.out.print("请输入一个数据：");
                    int number = input.nextInt();
                    queue.addQueue(number);
                    break;
                case 'g':
                    int res = queue.getQueue();
                    System.out.println("取出的数据为："+res);
                    break;
                case 'h':
                    int head = queue.headQueue();
                    System.out.println("取出的头部元素为："+head);
                    break;
                case 'e':
                    input.close();
                    loop = false;
                    break;
            }
        }
        System.out.println("程序退出！");
    }
}

class ArrayQueue{

    private int maxSize;//表示数组的最大容量
    private int front;//队列头
    private int rear;//队列尾
    private int[] arr;//数组用于存放数据 模拟队列

    public ArrayQueue(int arrMaxSize){
        maxSize = arrMaxSize;
        front = -1;//指向队列头部 指向队列头部的前一个位置
        rear = -1;//指向队列尾部 最后一个数据
        arr = new int[maxSize];
    }

    //判满
    public boolean isFull(){
        return rear == maxSize - 1;
    }

    //判空
    public boolean isEmpty(){
        return front == rear;
    }
    //添加数据到队列
    public void addQueue(int n){
        if(isFull()){
            System.out.println("队列已满，不允许插入元素！");
            return;
        }
        rear++;
        arr[rear] = n;
    }
    //获取队列的数据  出队列
    public int getQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列空，不能取数据");
        }
        front++;
        return arr[front];
    }
    //显示队列的所以数据
    public void shoeQueue(){
        if(isEmpty()){
            System.out.println("队列为空");
            return;
        }
        for(int i = 0;i<arr.length;i++){
            System.out.printf("arr[%d] = %d\n",i,arr[i]);
        }
    }

    //显示队列的头数据  不是取数据
    public int headQueue(){
        if(isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return arr[front + 1];
    }
}
