package com.itzhi.sparsearray;

public class SparseArray {

    public static void main(String[] args) {
        //稀疏数组的转化与还原
        int sparseArr1[][] = new int[11][11];

        sparseArr1[1][2] = 1;
        sparseArr1[2][3] = 2;

        for(int[] row : sparseArr1){
            for(int data : row){
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }

        int sum = 0;
        for(int i = 0;i<11;i++){
            for(int j = 0;j<11;j++){
                if(sparseArr1[i][j] != 0){
                    sum++;
                }
            }
        }
        System.out.println("sum="+sum);

        int sparseArr[][] = new int[sum+1][3];

        sparseArr[0][0] = 11;
        sparseArr[0][1] = 11;
        sparseArr[0][2] = sum;

        int count = 0;
        for(int i = 0;i<11;i++){
            for(int j = 0;j<11;j++){
                if(sparseArr1[i][j] != 0){
                    count++;
                    sparseArr[count][0] = i;
                    sparseArr[count][1] = j;
                    sparseArr[count][2] = sparseArr1[i][j];
                }
            }
        }

        for(int i = 0;i<3;i++){
            System.out.printf("%d\t%d\t%d\t\n",sparseArr[i][0],sparseArr[i][1],sparseArr[i][2]);
        }
        System.out.println("-----------------------------------------");


        //还原
        int chessArr2[][] = new int[sparseArr[0][0]][sparseArr[0][1]];

        for(int i = 1;i<sparseArr.length;i++){
            chessArr2[sparseArr[i][0]][sparseArr[i][1]] = sparseArr[i][2];
        }

        for(int[] row : chessArr2){
            for(int data : row){
                System.out.printf("%d\t",data);
            }
            System.out.println();
        }

    }
}
