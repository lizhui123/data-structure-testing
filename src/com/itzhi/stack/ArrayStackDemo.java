package com.itzhi.stack;

/*
* 数组模拟栈
* */
public class ArrayStackDemo {
    public static void main(String[] args) {

        ArrayStack stack = new ArrayStack(5);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.list();
        stack.pop();
        stack.list();
    }
}


class ArrayStack {

    private int maxSize;
    private int[] stack;
    private int top = -1;

    public ArrayStack(int maxSize) {

        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    // 判满
    public boolean isFull() {
        return top == maxSize - 1;
    }

    // 判空
    public boolean isEmpty() {
        return top == -1;
    }

    //添加
    public void push(int value) {

        if (isFull()) {
            System.out.println("已满，无法添加");
            return;
        }

        top++;
        stack[top] = value;
    }

    //删除
    public int pop() {

        if (isEmpty()) {
            throw new RuntimeException("栈为空");
        }

        int value = stack[top];
        top--;
        return value;
    }

    //展示
    public void list() {

        if (isEmpty()) {
            System.out.println("栈为空");
            return;
        }

        for (int i = top; i >= 0; i--) {
            System.out.println("stack[" + i + "]=" + stack[i]);
        }
    }
}